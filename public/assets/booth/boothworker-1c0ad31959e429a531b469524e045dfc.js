/*
 * JavaScript HTML 5 Worker for BOOTH
 */

// import needed resources
importScripts("/assets/lib/underscore-min.js");

importScripts("/assets/lib/jscrypto/jsbn.js",
	      "/assets/lib/jscrypto/jsbn2.js",
	      "/assets/lib/jscrypto/sjcl.js",
	      "/assets/lib/jscrypto/class.js",
	      "/assets/lib/jscrypto/bigint.js",
	      "/assets/lib/jscrypto/random.js",
	      "/assets/lib/jscrypto/elgamal.js",
	      "/assets/lib/jscrypto/sha1.js",
	      "/assets/lib/jscrypto/sha2.js",
	      "/assets/lib/jscrypto/helios.js");

var console = {
    'log' : function(msg) {
	self.postMessage({'type':'log','msg':msg});
    }
};

var ELECTION = null;
var Q_NUM = null;

function do_setup(message) {
    console.log("setting up worker " + message.question_num);

    ELECTION = HELIOS.Election.fromJSONString(message.election);
    Q_NUM = message.question_num;
}

function do_encrypt(message) {
    console.log("encrypting answer for question " + ELECTION.questions[Q_NUM]);

    var encrypted_answer = new HELIOS.EncryptedAnswer(ELECTION.questions[Q_NUM], message.answer, ELECTION.public_key);

    console.log("done encrypting");

    // send the result back
    self.postMessage({
	    'type': 'result',
		'encrypted_answer': encrypted_answer.toJSONObject(true),
		'id':message.id
		});
}

// receive either
// a) an election and an integer position of the question
// that this worker will be used to encrypt
// {'type': 'setup', 'question_num' : 2, 'election' : election_json}
//
// b) an answer that needs encrypting
// {'type': 'encrypt', 'answer' : answer_json}
//
self.onmessage = function(event) {
    // dispatch to method
    self['do_' + event.data.type](event.data);
}
;
