class CreateCastVotes < ActiveRecord::Migration
  def change
    create_table :cast_votes do |t|
      t.date :cast_at
      t.string :vote_hash
      t.string :voter_hash
      t.string :voter_uuid

      t.timestamps
    end
  end
end
