class CreateElections < ActiveRecord::Migration
  def change
    create_table :elections do |t|
      t.belongs_to :public_key
      t.string :cast_url
      t.string :description
      t.date :frozen_at
      t.string :name
      t.boolean :openreg
      t.string :short_name
      t.boolean :use_voter_aliases
      t.string :uuid
      t.string :voters_hash
      t.date :voting_ends_at
      t.date :voting_starts_at
      
      t.timestamps
    end
  end
end
