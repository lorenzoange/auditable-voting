class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.belongs_to :election
      t.integer :max
      t.integer :min
      t.string :question
      t.string :short_name
      t.string :choice_type
      t.string :result_type
      t.string :tally_type

      t.timestamps
    end
  end
end
