class CreateCiphertexts < ActiveRecord::Migration
  def change
    create_table :ciphertexts do |t|
      t.belongs_to :answer
      t.belongs_to :encrypted_answer
      t.belongs_to :public_key
      t.string :alpha
      t.string :beta

      t.timestamps
    end
  end
end
