class CreateCommitments < ActiveRecord::Migration
  def change
    create_table :commitments do |t|
      t.belongs_to :zk_proof
      t.string :a
      t.string :b

      t.timestamps
    end
  end
end
