class CreateSecretKeys < ActiveRecord::Migration
  def change
    create_table :secret_keys do |t|
      t.string :p
      t.string :x

      t.timestamps
    end
  end
end
