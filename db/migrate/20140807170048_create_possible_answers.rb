class CreatePossibleAnswers < ActiveRecord::Migration
  def change
    create_table :possible_answers do |t|
      t.belongs_to :question
      t.string :answer
      t.string :url

      t.timestamps
    end
  end
end
