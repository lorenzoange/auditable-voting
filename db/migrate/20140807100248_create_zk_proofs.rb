class CreateZkProofs < ActiveRecord::Migration
  def change
    create_table :zk_proofs do |t|
      t.belongs_to :individual_proof_for_encrypted_answer
      t.belongs_to :overall_proof_for_encrypted_answer
      t.string :challenge
      t.string :response

      t.timestamps
    end
  end
end
