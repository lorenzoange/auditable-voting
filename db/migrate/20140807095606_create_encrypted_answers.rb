class CreateEncryptedAnswers < ActiveRecord::Migration
  def change
    create_table :encrypted_answers do |t|
      t.belongs_to :vote
      t.timestamps
    end
  end
end
