class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.belongs_to :cast_vote
      t.string :election_hash
      t.string :election_uuid

      t.timestamps
    end
  end
end
