class CreatePublicKeys < ActiveRecord::Migration
  def change
    create_table :public_keys do |t|
      t.string :p
      t.string :g
      t.string :q
      t.string :y

      t.timestamps
    end
  end
end
