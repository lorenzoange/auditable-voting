# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140807170048) do

  create_table "cast_votes", force: true do |t|
    t.integer  "election_id"
    t.date     "cast_at"
    t.text   "vote_hash"
    t.text   "voter_hash"
    t.text   "voter_uuid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ciphertexts", force: true do |t|
    t.integer  "encrypted_answer_id"
    t.integer  "public_key_id"
    t.text   "alpha"
    t.text   "beta"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commitments", force: true do |t|
    t.integer  "zk_proof_id"
    t.text   "a"
    t.text   "b"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "elections", force: true do |t|
    t.integer  "public_key_id"
    t.string   "cast_url"
    t.text   "description"
    t.date     "frozen_at"
    t.string   "name"
    t.boolean  "openreg"
    t.string   "short_name"
    t.boolean  "use_voter_aliases"
    t.text   "uuid"
    t.text   "voters_hash"
    t.date     "voting_ends_at"
    t.date     "voting_starts_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "encrypted_answers", force: true do |t|
    t.integer  "vote_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "possible_answers", force: true do |t|
    t.integer  "question_id"
    t.string   "answer"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "public_keys", force: true do |t|
    t.text   "p"
    t.text   "g"
    t.text   "q"
    t.text   "y"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "questions", force: true do |t|
    t.integer  "election_id"
    t.integer  "max"
    t.integer  "min"
    t.string   "question"
    t.string   "short_name"
    t.string   "choice_type"
    t.string   "result_type"
    t.string   "tally_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "secret_keys", force: true do |t|
    t.text   "p"
    t.text   "x"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "votes", force: true do |t|
    t.integer  "cast_vote_id"
    t.text   "election_hash"
    t.text   "election_uuid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zk_proofs", force: true do |t|
    t.integer  "individual_proof_for_encrypted_answer_id"
    t.integer  "overall_proof_for_encrypted_answer_id"
    t.text   "challenge"
    t.text   "response"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
