require 'rational'

module MyMath
    
    module_function
    
    def expmod(base, exp, m)
      if exp == 0
        1
      elsif exp & 0b1 == 0 # even
        n = expmod(base, exp.div(2), m)
        (n * n) % m
      else # odd
        (base * expmod(base, exp - 1, m)) % m
      end
    end
    
    def inverse(u, v)
      #inverse(u:long, u:long):long
      #Return the inverse of u mod v.
      
      #assegnando u e v ad altre variabili l'algoritmo sembra molto piu veloce
      u3, v3 = u, v
      u1, v1 = 1, 0
      while v3 > 0 do
          q=u3 / v3
          u1, v1 = v1, u1 - v1*q
          u3, v3 = v3, u3 - v3*q
      end
      while u1<0 do
          u1 = u1 + v
      end
      return u1
    end
      
end
