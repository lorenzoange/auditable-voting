require 'test_helper'

class EncryptedAnswerTest < ActiveSupport::TestCase
  test "creation" do
    ea = EncryptedAnswer.create!
    
    ea.choices.create!(alpha: "alpha", beta: "beta", public_key: public_keys(:one))
    ea.choices.create!(alpha: "alpha1", beta: "beta1", public_key: public_keys(:one))
      
    ea.individual_proofs.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    ea.individual_proofs.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    
    ea.overall_proof = ZkProof.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    
    assert_equal ea.choices.length, 2
    assert_equal ea.individual_proofs.length, 2
    assert_not_nil ea.overall_proof
  end
end
