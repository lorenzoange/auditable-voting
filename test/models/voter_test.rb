require 'test_helper'

class VoterTest < ActiveSupport::TestCase
  test "creation" do
    v = Voter.create!(name: "name", uuid: "uuid")
    
    assert_not_nil v.name
    assert_not_nil v.uuid
  end
end
