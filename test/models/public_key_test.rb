require 'test_helper'

class PublicKeyTest < ActiveSupport::TestCase
  test "creation" do
    pk = PublicKey.create!(g: "g", p: "p", q: "q", y: "y")
    
    assert_not_nil pk.g
    assert_not_nil pk.p
    assert_not_nil pk.q
    assert_not_nil pk.y
  end
  
  test "encrypt" do
    pk = public_keys(:one)
    
    assert_not_nil pk.encrypt(3)
  end
end
