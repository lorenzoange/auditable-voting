require 'test_helper'

class ZkProofTest < ActiveSupport::TestCase
  test "creation" do
    
    zkp = ZkProof.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    
    assert_not_nil zkp.challenge
    assert_not_nil zkp.commitment
    assert_not_nil zkp.response
  end
end
