require 'test_helper'

class PossibleAnswerTest < ActiveSupport::TestCase
  test "creation" do
    pa = PossibleAnswer.create!(answer: "answer1", url: "url1")
    
    assert_not_nil pa.answer
    assert_not_nil pa.url
  end
end
