require 'test_helper'

class CastVoteTest < ActiveSupport::TestCase
  test "creation" do
    #v = Vote.create!(election_hash: "hash", election_uuid: "uuid")
    
    #v.encrypted_answers.create!
    #pk = PublicKey.create!(g: "g", p: "p", q: "q", y: "y")
    #v.encrypted_answers[0].choices.create!(alpha: "alpha", beta: "beta", public_key: pk)
    #v.encrypted_answers[0].choices.create!(alpha: "alpha1", beta: "beta1", public_key: pk)
    
    #c = Commitment.create!(a: "987654321", b: "987654321")
     
    #v.encrypted_answers[0].individual_proofs.create!(challenge: "987654321", commitment: c, response: "987654321")
    #v.encrypted_answers[0].individual_proofs.create!(challenge: "987654321", commitment: c, response: "987654321")
    
    #v.encrypted_answers[0].overall_proof = ZkProof.create!(challenge: "987654321", commitment: c, response: "987654321")
    
    cv = CastVote.create!(cast_at: Time.now, vote: votes(:one), vote_hash: "vote_hash", voter_hash: "voter_hash", voter_uuid: "voter_uuid")
    
    assert_not_nil cv.cast_at
    assert_not_nil cv.vote
    assert_not_nil cv.vote_hash
    assert_not_nil cv.voter_hash
    assert_not_nil cv.voter_uuid

  end
end
