require 'test_helper'

class CommitmentTest < ActiveSupport::TestCase
  test "creation" do
    c = Commitment.create!(a: "987654321", b: "987654321")
    assert_not_nil c.a
    assert_not_nil c.b
  end
end
