require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  test "creation" do
   
    q = Question.create!(question: "question", short_name: "short_name")
     
    q.possible_answers.create!(answer: "answer1", url: "url1")
    q.possible_answers.create!(answer: "answer2", url: "url2")

    assert_equal q.possible_answers.length, 2
    assert_not_nil q.question
    assert_not_nil q.short_name
    
    assert_not_nil q.min
    assert_not_nil q.max
    assert_not_nil q.choice_type
    assert_not_nil q.result_type
    assert_not_nil q.tally_type            
  end
end
