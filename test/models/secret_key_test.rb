require 'test_helper'

class SecretKeyTest < ActiveSupport::TestCase
  test "creation" do
    sk = SecretKey.create!(p: "p", x: "x")
    
    assert_not_nil sk.p
    assert_not_nil sk.x
  end
  
  test "decrypt" do
    pk = public_keys(:one)
    sk = secret_keys(:one)
    
    assert_equal 44, sk.decrypt(pk.encrypt(44))
  end
  
  test "prove decryption" do
    pk = public_keys(:one)
    sk = secret_keys(:one)
    
    assert_equal sk.prove_decryption(pk.encrypt(44)).length, 2
    assert_equal 44, sk.prove_decryption(pk.encrypt(44))[0]

  end
end
