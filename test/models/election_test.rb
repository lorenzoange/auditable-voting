require 'test_helper'

class ElectionTest < ActiveSupport::TestCase
  test "creation" do
  
    e = Election.create!(cast_url: "cast_url", description: "description", frozen_at: Time.now, name: "name", public_key: public_keys(:one),
                          use_voter_aliases: false, uuid: "uuid", voters_hash: "voters_hash", openreg:true, voting_ends_at: nil, voting_starts_at: nil)
    e.questions.create!(question: "question", short_name: "short_name")
     
    e.questions[0].possible_answers.create!(answer: "answer1", url: "url1")
    e.questions[0].possible_answers.create!(answer: "answer2", url: "url2")   
  
    assert_not_nil e.cast_url
    assert_not_nil e.description
    assert_not_nil e.frozen_at
    assert_not_nil e.name
    assert_not_nil e.public_key
    assert_not_nil e.use_voter_aliases
    assert_not_nil e.uuid
    assert_not_nil e.voters_hash
    assert_not_nil e.openreg
    assert_equal e.questions.length, 1
  
  end
end
