require 'test_helper'

class CiphertextTest < ActiveSupport::TestCase
  test "creation" do

    c = Ciphertext.create!(alpha: "alpha", beta: "beta")
    c.public_key = public_keys(:one)
    
    assert_not_nil c.alpha
    assert_not_nil c.beta
    assert_not_nil c.public_key
  end
end
