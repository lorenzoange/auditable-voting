require 'test_helper'

class VoteTest < ActiveSupport::TestCase
  test "creation" do
    v = Vote.create!(election_hash: "hash", election_uuid: "uuid")
    
    v.encrypted_answers.create!
    v.encrypted_answers[0].choices.create!(alpha: "alpha", beta: "beta", public_key: public_keys(:one))
    v.encrypted_answers[0].choices.create!(alpha: "alpha1", beta: "beta1", public_key: public_keys(:one))
    v.encrypted_answers[0].individual_proofs.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    v.encrypted_answers[0].individual_proofs.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    v.encrypted_answers[0].overall_proof = ZkProof.create!(challenge: "987654321", commitment: commitments(:one), response: "987654321")
    
    assert_not_nil v.election_hash
    assert_not_nil v.election_uuid
    assert_equal v.encrypted_answers[0].choices.length, 2
  end
end
