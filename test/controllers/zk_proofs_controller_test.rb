require 'test_helper'

class ZkProofsControllerTest < ActionController::TestCase
  setup do
    @zk_proof = zk_proofs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:zk_proofs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create zk_proof" do
    assert_difference('ZkProof.count') do
      post :create, zk_proof: { challenge: @zk_proof.challenge, response: @zk_proof.response }
    end

    assert_redirected_to zk_proof_path(assigns(:zk_proof))
  end

  test "should show zk_proof" do
    get :show, id: @zk_proof
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @zk_proof
    assert_response :success
  end

  test "should update zk_proof" do
    patch :update, id: @zk_proof, zk_proof: { challenge: @zk_proof.challenge, response: @zk_proof.response }
    assert_redirected_to zk_proof_path(assigns(:zk_proof))
  end

  test "should destroy zk_proof" do
    assert_difference('ZkProof.count', -1) do
      delete :destroy, id: @zk_proof
    end

    assert_redirected_to zk_proofs_path
  end
end
