require 'test_helper'

class EncryptedAnswersControllerTest < ActionController::TestCase
  setup do
    @encrypted_answer = encrypted_answers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:encrypted_answers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create encrypted_answer" do
    assert_difference('EncryptedAnswer.count') do
      post :create, encrypted_answer: {  }
    end

    assert_redirected_to encrypted_answer_path(assigns(:encrypted_answer))
  end

  test "should show encrypted_answer" do
    get :show, id: @encrypted_answer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @encrypted_answer
    assert_response :success
  end

  test "should update encrypted_answer" do
    patch :update, id: @encrypted_answer, encrypted_answer: {  }
    assert_redirected_to encrypted_answer_path(assigns(:encrypted_answer))
  end

  test "should destroy encrypted_answer" do
    assert_difference('EncryptedAnswer.count', -1) do
      delete :destroy, id: @encrypted_answer
    end

    assert_redirected_to encrypted_answers_path
  end
end
