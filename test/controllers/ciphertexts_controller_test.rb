require 'test_helper'

class CiphertextsControllerTest < ActionController::TestCase
  setup do
    @ciphertext = ciphertexts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ciphertexts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ciphertext" do
    assert_difference('Ciphertext.count') do
      post :create, ciphertext: { alpha: @ciphertext.alpha, beta: @ciphertext.beta }
    end

    assert_redirected_to ciphertext_path(assigns(:ciphertext))
  end

  test "should show ciphertext" do
    get :show, id: @ciphertext
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ciphertext
    assert_response :success
  end

  test "should update ciphertext" do
    patch :update, id: @ciphertext, ciphertext: { alpha: @ciphertext.alpha, beta: @ciphertext.beta }
    assert_redirected_to ciphertext_path(assigns(:ciphertext))
  end

  test "should destroy ciphertext" do
    assert_difference('Ciphertext.count', -1) do
      delete :destroy, id: @ciphertext
    end

    assert_redirected_to ciphertexts_path
  end
end
