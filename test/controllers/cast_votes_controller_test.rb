require 'test_helper'

class CastVotesControllerTest < ActionController::TestCase
  setup do
    @cast_vote = cast_votes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cast_votes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cast_vote" do
    assert_difference('CastVote.count') do
      post :create, cast_vote: { cast_at: @cast_vote.cast_at, vote_hash: @cast_vote.vote_hash, voter_hash: @cast_vote.voter_hash, voter_uuid: @cast_vote.voter_uuid }
    end

    assert_redirected_to cast_vote_path(assigns(:cast_vote))
  end

  test "should show cast_vote" do
    get :show, id: @cast_vote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cast_vote
    assert_response :success
  end

  test "should update cast_vote" do
    patch :update, id: @cast_vote, cast_vote: { cast_at: @cast_vote.cast_at, vote_hash: @cast_vote.vote_hash, voter_hash: @cast_vote.voter_hash, voter_uuid: @cast_vote.voter_uuid }
    assert_redirected_to cast_vote_path(assigns(:cast_vote))
  end

  test "should destroy cast_vote" do
    assert_difference('CastVote.count', -1) do
      delete :destroy, id: @cast_vote
    end

    assert_redirected_to cast_votes_path
  end
end
