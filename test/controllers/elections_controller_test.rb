require 'test_helper'

class ElectionsControllerTest < ActionController::TestCase
  setup do
    @election = elections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:elections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create election" do
    assert_difference('Election.count') do
      post :create, election: { cast_url: @election.cast_url, description: @election.description, frozen_at: @election.frozen_at, name: @election.name, openreg: @election.openreg, short_name: @election.short_name, use_voter_aliases: @election.use_voter_aliases, uuid: @election.uuid, voters_hash: @election.voters_hash, voting_ends_at: @election.voting_ends_at, voting_starts_at: @election.voting_starts_at }
    end

    assert_redirected_to election_path(assigns(:election))
  end

  test "should show election" do
    get :show, id: @election
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @election
    assert_response :success
  end

  test "should update election" do
    patch :update, id: @election, election: { cast_url: @election.cast_url, description: @election.description, frozen_at: @election.frozen_at, name: @election.name, openreg: @election.openreg, short_name: @election.short_name, use_voter_aliases: @election.use_voter_aliases, uuid: @election.uuid, voters_hash: @election.voters_hash, voting_ends_at: @election.voting_ends_at, voting_starts_at: @election.voting_starts_at }
    assert_redirected_to election_path(assigns(:election))
  end

  test "should destroy election" do
    assert_difference('Election.count', -1) do
      delete :destroy, id: @election
    end

    assert_redirected_to elections_path
  end
  
end
