json.answers vote.encrypted_answers do |ea|
  json.choices ea.choices do |choice|
    json.extract! choice, :alpha, :beta
  end
  
  couples = ea.individual_proofs.each_slice(2).to_a
    
  json.individual_proofs couples do |couple|
    
    json.array! couple do |ip|
      json.partial! ip
    end
    
  end
  
  json.overall_proof ea.overall_proofs do |op|
    json.partial! op
  end
end
json.extract! vote, :election_hash, :election_uuid
