json.array! @trustees do |trustee|
  json.extract! trustee, :decryption_factors
  json.decryption_proofs do
    json.array! trustee[:decryption_proofs] do |q|
      json.array! q do |zk_proof|
          json.partial! zk_proof
      end
    end
  end
  json.extract! trustee, :email, :pok
  json.public_key do
    json.partial! trustee[:public_key]
  end
  json.extract! trustee, :public_key_hash, :uuid
end
