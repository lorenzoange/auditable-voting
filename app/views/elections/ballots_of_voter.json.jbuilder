json.extract! @cast_vote, :cast_at
json.vote do
  json.partial! @cast_vote.vote
end
json.extract! @cast_vote, :vote_hash, :voter_hash, :voter_uuid

