json.extract! @election, :cast_url, :description, :frozen_at, :name, :openreg, :short_name, :use_voter_aliases, :uuid, :voters_hash, :voting_ends_at, :voting_starts_at
json.public_key do
  json.partial! @election.public_key
end
json.questions @election.questions do |question|
  json.extract! question, :answers, :answer_urls, :choice_type, :max, :min, :question, :short_name, :result_type, :tally_type
end
