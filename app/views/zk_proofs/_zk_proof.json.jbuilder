json.extract! zk_proof, :challenge
json.commitment do
  json.A zk_proof.commitment.a
  json.B zk_proof.commitment.b
end
json.extract! zk_proof, :response
