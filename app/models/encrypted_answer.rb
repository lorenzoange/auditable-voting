class EncryptedAnswer < ActiveRecord::Base
  belongs_to :vote
  has_many :choices, :class_name => "Ciphertext", autosave: true
  has_many :individual_proofs, :class_name => "ZkProof", :foreign_key => 'individual_proof_for_encrypted_answer_id', autosave: true
  has_many :overall_proofs, :class_name => "ZkProof", :foreign_key => 'overall_proof_for_encrypted_answer_id', autosave: true
  
  def self.from_booth( encrypted_answer, public_key )
    ea = EncryptedAnswer.new
    
    encrypted_answer["choices"].each do |choice|
      c = Ciphertext.new
      c.attributes = choice
      c.public_key = public_key
      ea.choices << c
    end
    
    encrypted_answer["individual_proofs"].each do |individual_proof|
      #sono sempre due
      individual_proof.each do |proof|
        zkp = ZkProof.from_booth proof
        ea.individual_proofs << zkp
      end
    end
    
    encrypted_answer["overall_proof"].each do |overall_proof|
      zkp = ZkProof.from_booth overall_proof
      ea.overall_proofs << zkp
    end
    
    return ea
    
  end
end
