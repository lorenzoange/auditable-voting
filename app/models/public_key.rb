require 'my_math'
class PublicKey < ActiveRecord::Base
  
  [:g, :p, :q, :y ].each do |var|
    n = "#{var.to_s}_i"
    define_method(n) do
		  return send(var).to_i
  	end
  end
  
  def encrypt(n)
    r = rand(p_i - 1) + 1
    alpha = MyMath.expmod(g_i, r, p_i)
    y1 = MyMath.expmod(y_i, r, p_i)
    beta = MyMath.expmod(n * y1, 1, p_i)
    c = Ciphertext.new(alpha: alpha.to_s, beta: beta.to_s)
    c.public_key = self
    return c
  end

  def to_booth_json
    ac = ApplicationController.new                                                                                                                                                
    ac.instance_variable_set("@public_key", self)                                                                                                                  
    return ac.render_to_string( template: 'public_keys/show.json.jbuilder')
  end
  
  def to_booth_hash
    return Digest::SHA2.new.update( to_booth_json ).base64digest.chomp('=')
  end
  
  def verify_decryption_proof(cip, m, proof)
    # given g, y, alpha, beta/(encoded m), prove equality of discrete log
    # with Chaum Pedersen, and that discrete log is x, the secret key.

    # Prover sends a=g^w, b=alpha^w for random w
    # Challenge c = sha1(a,b) with and b in decimal form
    # Prover sends t = w + xc

    # Verifier will check that g^t = a * y^c
    # and alpha^t = b * beta/m ^ c
    
    g = self.g_i
    t = proof.response_i
    a = proof.commitment.a_i
    b = proof.commitment.b_i
    y = self.y_i
    q = self.q_i
    p = self.p_i
    c = Digest::SHA1.new.update(a.to_s + "," + b.to_s).to_s.to_i(16)

    left = MyMath.expmod(g, t, p)
    right = ( a*MyMath.expmod(y, c, p) ) %p
    #my_puts "g^t = a * y^c -> "+(left == right).to_s 

    left2 = MyMath.expmod(cip.alpha_i, t, p)
    beta_over_m = ( cip.beta_i * MyMath.inverse(m, p) ) %p
    right2 = (MyMath.expmod(beta_over_m, c, p) * b) % p
    #my_puts "alpha^t = b * beta/m ^ c -> "+(left2 == right2).to_s 
    
    return left == right && left2 == right2
  end


end
