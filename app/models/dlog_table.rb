class DlogTable
  #Keeping track of discrete logs
  
  def initialize(base, modulus)
    @dlogs = {}
    @dlogs[1] = 0
    @last_dlog_result = 1
    @counter = 0
    
    @base = base
    @modulus = modulus
  end
    
  def increment
    @counter += 1
    
    # new value
    new_value = (@last_dlog_result * @base) % @modulus
    
    # record the discrete log
    @dlogs[new_value] = @counter
    
    # record the last value
    @last_dlog_result = new_value
  end
    
  def precompute(up_to)
    while @counter < up_to do
      increment
    end
  end
  
  def lookup(value)
    return @dlogs[value]
  end
end
