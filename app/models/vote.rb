class Vote < ActiveRecord::Base
  belongs_to :cast_vote
  has_many :encrypted_answers, autosave: true
  
  def to_booth_json
    ac = ApplicationController.new                                                                                                                                                
    ac.instance_variable_set("@vote", self)                                                                                                                  
    return ac.render_to_string( template: 'votes/show.json.jbuilder')
  end
  
  def to_booth_hash
    return Digest::SHA2.new.update( to_booth_json ).base64digest.chomp('=')
  end

  
  def self.from_booth( encrypted_vote, public_key )
    v = Vote.new
    v.election_hash = encrypted_vote["election_hash"]
    v.election_uuid = encrypted_vote["election_uuid"]
    
    encrypted_vote["answers"].each do |encrypted_answer|
      ea = EncryptedAnswer.from_booth( encrypted_answer, public_key )
      v.encrypted_answers << ea
    end
    
    return v
    
  end
end
