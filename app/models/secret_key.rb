class SecretKey < ActiveRecord::Base

  [:p, :x ].each do |var|
    n = "#{var.to_s}_i"
    define_method(n) do
		  return send(var).to_i
  	end
  end
  
  def decrypt(ciphertext)
    s = MyMath.expmod(ciphertext.alpha_i,  p_i - 1 - x_i, p_i)
    ( ciphertext.beta_i * s ).modulo p_i
  end
  
  def decryption_factor(ciphertext)
    return MyMath.expmod(ciphertext.alpha_i, x_i, p_i)
  end
  
  def prove_decryption(ciphertext)
    # given g, y, alpha, beta/(encoded m), prove equality of discrete log
    # with Chaum Pedersen, and that discrete log is x, the secret key.

    # Prover sends a=g^w, b=alpha^w for random w
    # Challenge c = sha1(a,b) with and b in decimal form
    # Prover sends t = w + xc

    # Verifier will check that g^t = a * y^c
    # and alpha^t = b * beta/m ^ c
    
    return nil if ciphertext.public_key.p != p
    m = (ciphertext.beta_i * MyMath.inverse(MyMath.expmod(ciphertext.alpha_i, x_i, p_i), p_i)) % p_i
    
    w = rand(ciphertext.public_key.q_i)
    a = MyMath.expmod(ciphertext.public_key.g_i, w, p_i)
    b = MyMath.expmod(ciphertext.alpha_i, w, p_i)
    c = Digest::SHA1.new.update(a.to_s + "," + b.to_s).to_s.to_i(16)
    t = (w + x_i * c) % ciphertext.public_key.q_i
    
    zkp = ZkProof.new(challenge: c.to_s, response: t.to_s)
    zkp.commitment = Commitment.new(a: a.to_s, b: b.to_s)
    
    return [m, zkp]
  end
  
  def prove_of_knowledge public_key
    w = rand( public_key.q_i )
    commitment = MyMath.expmod(public_key.g_i, w, p_i)
    challenge = Digest::SHA1.new.update( commitment.to_s ).to_s.to_i(16) % public_key.q_i
    response = (w + (x_i * challenge)) % public_key.q_i
    return { challenge: challenge.to_s, commitment: commitment.to_s, response: response.to_s }
  end
end
