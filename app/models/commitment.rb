class Commitment < ActiveRecord::Base
  belongs_to :zk_proof
  
  # all'interno del db sono salvati come stringhe ma all'esterno li rappresento anche come interi
  # per poterli utilizzare nei conti
  [:a, :b ].each do |var|
    n = "#{var.to_s}_i"
    define_method(n) do
		  return send(var).to_i
  	end
  end
  
end
