class Question < ActiveRecord::Base
  belongs_to :election
  has_many :possible_answers
    
  after_initialize :defaults

  def defaults
    self.min ||= 0
    self.max ||= 1
    # i campi successivi sono stati lasciati per coerenza con helios 
    # ma attualmente non sono supportati valori diversi nè da helios nè da questa demo
    self.choice_type = "approval"
    self.result_type = "absolute"
    self.tally_type = "homomorphic"
  end
  
  def answer_urls
    answer_urls = []
    possible_answers.each do |p_a|
      answer_urls << p_a.url
    end
    return answer_urls
  end
  
  def answers
    answers = []
    possible_answers.each do |p_a|
      answers << p_a.answer
    end
    return answers
  end
end
