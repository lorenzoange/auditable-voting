class ZkProof < ActiveRecord::Base
  has_one :commitment, autosave: true
  belongs_to :encrypted_answer, :foreign_key => 'individual_proof_for_encrypted_answer_id'
  
  # all'interno dell'oggetto sono salvati come stringhe ma all'esterno li rappresento anche come interi
  # per poterli utilizzare nei conti
  [:challenge, :response ].each do |var|
    n = "#{var.to_s}_i"
    define_method(n) do
		  return send(var).to_i
  	end
  end
  
  def self.from_booth zk_proof

    zkp = ZkProof.new
    zkp.challenge = zk_proof["challenge"]
    zkp.response = zk_proof["response"]
    
    c = Commitment.new
    c.a = zk_proof["commitment"]["A"]
    c.b = zk_proof["commitment"]["B"]
    
    zkp.commitment = c
    
    return zkp
    
  end
end
