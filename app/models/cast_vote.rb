class CastVote < ActiveRecord::Base
  belongs_to :election
  has_one :vote, autosave: true
end
