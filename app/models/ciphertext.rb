class Ciphertext < ActiveRecord::Base
  #belongs_to :answer
  belongs_to :public_key
  belongs_to :encrypted_answer

  # all'interno del db sono salvati come stringhe ma all'esterno li rappresento anche come interi
  # per poterli utilizzare nei conti
  [:alpha, :beta ].each do |var|
    n = "#{var.to_s}_i"
    define_method(n) do
		  return send(var).to_i
  	end
  end
  
  def *(other)
    alpha1 = alpha_i*other.alpha_i % public_key.p_i
    beta1 = beta_i*other.beta_i % public_key.p_i
    Ciphertext.new( alpha: alpha1.to_s, beta: beta1.to_s, public_key: public_key )
  end
end
