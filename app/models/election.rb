class Election < ActiveRecord::Base
  has_many :questions, autosave: true
  has_many :cast_votes, autosave: true
  belongs_to :public_key
 
  after_initialize :defaults
    
  def defaults
    self.frozen_at ||= Time.now.to_s
    self.uuid ||= SecureRandom.uuid    

    # i campi successivi sono stati lasciati per coerenza con helios 
    # ma attualmente non sono supportati valori diversi da questa demo
    self.openreg = true
    self.use_voter_aliases = false
    self.voters_hash = nil
    self.voting_ends_at = nil
    self.voting_starts_at = nil

  end
 
  def cast_url
    return "../elections/#{self.id}/authenticate"
  end
 
  def to_booth_json
    ac = ApplicationController.new                                                                                                                                                
    ac.instance_variable_set("@election", self)                                                                                                                  
    return ac.render_to_string( template: 'elections/show.json.jbuilder')
  end
  
  def to_booth_hash
    return Digest::SHA2.new.update( to_booth_json ).base64digest.chomp('=')
  end
  
  # Per i dettagli sull'algoritmo per la verifica della correttezza del voto si rimanda alla documentazione ufficiale 
  # http://documentation.heliosvoting.org/verification-specs/helios-v3-verification-specs
  def verify_vote(vote)
    # check hash
    return false if vote.election_hash != to_booth_hash
    
    0.upto( vote.encrypted_answers.length-1 ) do |question_num|
      encrypted_answer = vote.encrypted_answers[question_num]
      question = self.questions[question_num]
      
      # initialize homomorphic sum
      homomorphic_sum = Ciphertext.new(alpha: "1", beta: "1", public_key: self.public_key)
      
      # go through each choice for the question
      0.upto( encrypted_answer.choices.length-1 ) do |choice_num|
        ciphertext = encrypted_answer.choices[choice_num]

        # ogni risposta possibile ha una coppia di ZKProof
        disjunctive_proof = []
        disjunctive_proof << encrypted_answer.individual_proofs[choice_num*2]
        disjunctive_proof << encrypted_answer.individual_proofs[choice_num*2+1]
        
        # check the individual proof (disjunctive max is 1)
        return false if ! verify_disjunctive_min_max_proof(ciphertext, 0, 1, disjunctive_proof, self.public_key)
        
        # keep track of homomorphic sum
        homomorphic_sum = ciphertext * homomorphic_sum
      end
      
      # check the overall proof   
      return false if ! verify_disjunctive_min_max_proof(homomorphic_sum, question.min, question.max,
                                            encrypted_answer.overall_proofs,
                                            self.public_key)
    end
    
    # done, we succeeded
    return true
  end
  
  # esegue la somma omomorfica dei voti e decifra la somma ottenuta fornendo opportuna prova di corretta decifrazione
  # result = [ [ { homomorphic_tally, decrypted_tally, decryption_proof }, ...], [...], [...], ... ]
  # un array per ogni domanda ( @result[0] fa riferimento alla prima domanda )
  # un array associativo di tre elementi ( [homomorphic_tally, decripted_tally, decription_proof] ) per ogni possibile risposta
  # homomorphic_tally is a Ciphertext, decripted_tally is a BigNum, decription_proof is a ZkProof
  def compute_tally(cast_votes, secret_key)
    
    result = []

    0.upto( self.questions.length-1 ) do |question|
      result << []
      0.upto( self.questions[question].answers.length-1 ) do |answer|
        
        res_crypt = cast_votes.map do |cast_vote| 
          cast_vote.vote.encrypted_answers[question].choices[answer] 
        end.inject(:*)
        
        res_decrypt = secret_key.prove_decryption(res_crypt)
        decryption_factor = secret_key.decryption_factor(res_crypt)
        
        res = { homomorphic_tally: res_crypt, decrypted_tally: res_decrypt[0], decryption_proof: res_decrypt[1], decryption_factor: decryption_factor }
        result[question] << res
        
      end
    end 
    
    return result
    
  end
  
  private

    # Prover sends A = g^w mod p and B = y^w mod p for a random w.
    # Verifier sends challenge, a random challenge mod q.
    # Prover sends response = w + challenge * r.
    # Verifier checks that:
    # g^response = A * alpha^challenge
    # y^response = B * (beta/g^m)^challenge
    def verify_proof(ciphertext, plaintext, proof, public_key)
          
      return false if MyMath.expmod(public_key.g_i, proof.response_i, public_key.p_i) != 
                      ((proof.commitment.a_i * MyMath.expmod(ciphertext.alpha_i, proof.challenge_i, public_key.p_i)) % public_key.p_i)
                      
      beta_over_m = ciphertext.beta_i * MyMath.inverse(MyMath.expmod(public_key.g_i, plaintext, public_key.p_i), public_key.p_i)
      beta_over_m_mod_p = beta_over_m % public_key.p_i

      return false if MyMath.expmod(public_key.y_i, proof.response_i, public_key.p_i) !=
                      ((proof.commitment.b_i * MyMath.expmod(beta_over_m_mod_p, proof.challenge_i, public_key.p_i)) % public_key.p_i)
      return true   
    end

    # NOTA BENE: rispetto all'algoritmo originale presente nella documentazione 
    #            questo algoritmo permette la verifica di un range di valori che
    #            comincia per un valore anche diverso da zero
    def verify_disjunctive_min_max_proof(ciphertext, min, max, disjunctive_proof, public_key)
      0.upto(max-min) do |i|
        # the proof for plaintext "i"
        return false if !verify_proof(ciphertext, i+min, disjunctive_proof[i], public_key)
      end
      
      # the overall challenge
      computed_challenge = disjunctive_proof.map{ |proof| proof.challenge_i }.inject(:+) % public_key.q_i
      
      # concatenate the arrays of A,B values
      list_of_values_to_hash = []
      disjunctive_proof.each do |p|
        list_of_values_to_hash << p.commitment.a_i
        list_of_values_to_hash << p.commitment.b_i
      end
      
      # concatenate as strings
      str_to_hash = list_of_values_to_hash.join ","

      #hash
      expected_challenge = Digest::SHA1.new.update(str_to_hash).to_s.to_i(16)
      #last check
      return expected_challenge == computed_challenge
    end
end
