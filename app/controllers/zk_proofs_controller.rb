class ZkProofsController < ApplicationController
  before_action :set_zk_proof, only: [:show, :edit, :update, :destroy]

  # GET /zk_proofs
  # GET /zk_proofs.json
  def index
    @zk_proofs = ZkProof.all
  end

  # GET /zk_proofs/1
  # GET /zk_proofs/1.json
  def show
  end

  # GET /zk_proofs/new
  def new
    @zk_proof = ZkProof.new
  end

  # GET /zk_proofs/1/edit
  def edit
  end

  # POST /zk_proofs
  # POST /zk_proofs.json
  def create
    @zk_proof = ZkProof.new(zk_proof_params)

    respond_to do |format|
      if @zk_proof.save
        format.html { redirect_to @zk_proof, notice: 'Zk proof was successfully created.' }
        format.json { render action: 'show', status: :created, location: @zk_proof }
      else
        format.html { render action: 'new' }
        format.json { render json: @zk_proof.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /zk_proofs/1
  # PATCH/PUT /zk_proofs/1.json
  def update
    respond_to do |format|
      if @zk_proof.update(zk_proof_params)
        format.html { redirect_to @zk_proof, notice: 'Zk proof was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @zk_proof.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /zk_proofs/1
  # DELETE /zk_proofs/1.json
  def destroy
    @zk_proof.destroy
    respond_to do |format|
      format.html { redirect_to zk_proofs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_zk_proof
      @zk_proof = ZkProof.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def zk_proof_params
      params.require(:zk_proof).permit(:challenge, :response)
    end
end
