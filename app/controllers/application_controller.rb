class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def index
    @elections = Election.all
    #redirect_to booth_vote_path :election_url => "../elections/1.json"
  end
  
end
