class ElectionsController < ApplicationController
  before_action :set_election, only: [ :show, :edit, :update, :destroy, :cast_vote, :compute_tally, :bulletin_board, 
                                       :voters, :ballots, :ballots_of_voter, :result, :trustees ]
  skip_before_action :verify_authenticity_token, only: :authenticate
  
  # GET /elections/1
  def show
    render 'elections/show.json'
  end

  # GET /elections/new
  def new
  end

  # POST /elections/create
  def create
    @election = Election.create!(description: params['description'], 
                                 name: params['name'], 
                                 short_name: params['short_name'], 
                                 public_key: PublicKey.find(1) )
                                 
    0.upto( params['n_questions'].to_i - 1 ) do |n_question|
      @election.questions.create!(question: params["question_#{n_question}_name"], 
                                  short_name: params["question_#{n_question}_short_name"],
                                  min: params["question_#{n_question}_min"].to_i,
                                  max: params["question_#{n_question}_max"].to_i)
                                  
      0.upto( params["question_#{n_question}_n_possible_answers"].to_i - 1 ) do |n_answer|
        @election.questions[n_question].possible_answers.create!(answer: params["possible_answer_#{n_question}_#{n_answer}_answer"], 
                                                                 url: params["possible_answer_#{n_question}_#{n_answer}_url"])
      end
    end
  end
    
  # POST /elections/1/authenticate
  def authenticate
  end
  
  # POST /elections/1/cast_vote
  def cast_vote

    # prevent ballot stuffing
    cast_votes = CastVote.where("election_id = ?", @election.id)  
    cast_votes.each do |cast_vote|
      if cast_vote.voter_uuid == params['uuid'] then
        @result = "Errore: l'utente #{params['uuid']} ha già votato in questa elezione"
        return
      end
    end
    
    vote_hash = Digest::SHA2.new.update(params[:encrypted_vote]).base64digest.chomp('=')
    
    @cv = CastVote.new
    @cv.cast_at = Time.now
    @cv.vote_hash = vote_hash
    # l'hash del voter è effettuato soltanto sull'uuid...
    @cv.voter_hash = Digest::SHA2.new.update(params[:uuid]).base64digest.chomp('=')
    @cv.voter_uuid = params[:uuid]
        
    v = Vote.from_booth( JSON.parse( params[:encrypted_vote] ),  @election.public_key)
    
    if v.to_booth_hash == vote_hash && @election.verify_vote( v ) then
      @cv.vote = v
      @election.cast_votes << @cv
      @result = "Voto inserito correttamente"
    else
      @result = "Errore: voto malformato"
    end
        
  end
  
  # GET /elections/1/bulletin_board
  def bulletin_board
    @bulletin_board = []
    @election.cast_votes.each do |cast_vote|
      @bulletin_board << { voter_uuid: cast_vote.voter_uuid, vote_hash: cast_vote.vote_hash }
    end
    @election_hash = @election.to_booth_hash
  end

  # GET /elections/1/voters[?limit=1][&after=1]
  def voters
    @voters = []
    
    count = 1
    is_after_uuid = params[:after] ? false : true
    
    @election.cast_votes.each do |cast_vote|
      
      if( is_after_uuid ) then
        @voters << { election_uuid: @election.uuid, 
                     name: "No Name", 
                     uuid: cast_vote.voter_uuid, 
                     voter_id_hash: cast_vote.voter_hash, 
                     voter_type: "none" }
        
        break if params[:limit] && count >= params[:limit].to_i
        count += 1
      end
      
      is_after_uuid = true if( !is_after_uuid && cast_vote.voter_uuid == params[:after] )
      
    end
        
    render 'voters.json'
  end
  
  # GET /elections/1/ballots 
  def ballots
    @ballots = []
    @election.cast_votes.each do |cast_vote|
      @ballots << { cast_at: cast_vote.cast_at,
                    vote_hash: cast_vote.vote_hash,
                    voter_hash: cast_vote.voter_hash,
                    voter_uuid: cast_vote.voter_uuid, }
    end
    render 'ballots.json'
  end

  # GET /elections/1/voters/1/last   
  def ballots_of_voter
    
    @election.cast_votes.each do |cast_vote|
      
      if( cast_vote.voter_uuid == params[:voter_uuid] ) then
        @cast_vote = cast_vote
        render 'ballots_of_voter.json' and return
      end
    end
    render nothing: true
  end

  # GET /elections/1/result 
  def result
    cast_votes = CastVote.where("election_id = ?", @election.id)

    render nothing: true and return if cast_votes.length == 0
    
    risultato = @election.compute_tally(cast_votes, SecretKey.find(1))
    
    dlog_table = DlogTable.new(@election.public_key.g_i, @election.public_key.p_i)
    dlog_table.precompute( cast_votes.length )
    
    @votes_count = []
    risultato.each do |q|
      @votes_count << []
      q.each do |a|
        @votes_count[-1] << dlog_table.lookup( a[:decrypted_tally] )
      end
    end
    render 'result.json'  
  end

  # GET /elections/1/trustees  
  def trustees
    cast_votes = CastVote.where("election_id = ?", @election.id)

    render nothing: true and return if cast_votes.length == 0
    
    secret_key = SecretKey.find(1)
    
    result = @election.compute_tally(cast_votes, secret_key)
    
    decryption_factors = []
    decryption_proofs = []
    result.each do |q|
      decryption_factors << []
      decryption_proofs << []
      q.each do |a|
        decryption_factors[-1] << a[:decryption_factor].to_s
        decryption_proofs[-1] << a[:decryption_proof]
      end
    end
    
    email = "demo_trustee_email"
    pok = secret_key.prove_of_knowledge @election.public_key
    public_key = @election.public_key
    public_key_hash = @election.public_key.to_booth_hash
    uuid = "demo_trustee_uuid"
    @trustees = [{ decryption_factors: decryption_factors,
                   decryption_proofs: decryption_proofs, 
                   email: email,
                   pok: pok,
                   public_key: public_key,
                   public_key_hash: public_key_hash,
                   uuid: uuid }]
    render 'trustees.json'  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_election
      @election = Election.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def election_params
      params.require(:election).permit(:cast_url, :description, :frozen_at, :name, :openreg, :short_name, :use_voter_aliases, :uuid, :voters_hash, :voting_ends_at, :voting_starts_at)
    end
end
