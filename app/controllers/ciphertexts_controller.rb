class CiphertextsController < ApplicationController
  before_action :set_ciphertext, only: [:show, :edit, :update, :destroy]

  # GET /ciphertexts
  # GET /ciphertexts.json
  def index
    @ciphertexts = Ciphertext.all
  end

  # GET /ciphertexts/1
  # GET /ciphertexts/1.json
  def show
  end

  # GET /ciphertexts/new
  def new
    @ciphertext = Ciphertext.new
  end

  # GET /ciphertexts/1/edit
  def edit
  end

  # POST /ciphertexts
  # POST /ciphertexts.json
  def create
    @ciphertext = Ciphertext.new(ciphertext_params)

    respond_to do |format|
      if @ciphertext.save
        format.html { redirect_to @ciphertext, notice: 'Ciphertext was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ciphertext }
      else
        format.html { render action: 'new' }
        format.json { render json: @ciphertext.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ciphertexts/1
  # PATCH/PUT /ciphertexts/1.json
  def update
    respond_to do |format|
      if @ciphertext.update(ciphertext_params)
        format.html { redirect_to @ciphertext, notice: 'Ciphertext was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ciphertext.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ciphertexts/1
  # DELETE /ciphertexts/1.json
  def destroy
    @ciphertext.destroy
    respond_to do |format|
      format.html { redirect_to ciphertexts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ciphertext
      @ciphertext = Ciphertext.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ciphertext_params
      params.require(:ciphertext).permit(:alpha, :beta)
    end
end
