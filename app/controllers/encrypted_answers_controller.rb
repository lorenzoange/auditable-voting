class EncryptedAnswersController < ApplicationController
  before_action :set_encrypted_answer, only: [:show, :edit, :update, :destroy]

  # GET /encrypted_answers
  # GET /encrypted_answers.json
  def index
    @encrypted_answers = EncryptedAnswer.all
  end

  # GET /encrypted_answers/1
  # GET /encrypted_answers/1.json
  def show
  end

  # GET /encrypted_answers/new
  def new
    @encrypted_answer = EncryptedAnswer.new
  end

  # GET /encrypted_answers/1/edit
  def edit
  end

  # POST /encrypted_answers
  # POST /encrypted_answers.json
  def create
    @encrypted_answer = EncryptedAnswer.new(encrypted_answer_params)

    respond_to do |format|
      if @encrypted_answer.save
        format.html { redirect_to @encrypted_answer, notice: 'Encrypted answer was successfully created.' }
        format.json { render action: 'show', status: :created, location: @encrypted_answer }
      else
        format.html { render action: 'new' }
        format.json { render json: @encrypted_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /encrypted_answers/1
  # PATCH/PUT /encrypted_answers/1.json
  def update
    respond_to do |format|
      if @encrypted_answer.update(encrypted_answer_params)
        format.html { redirect_to @encrypted_answer, notice: 'Encrypted answer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @encrypted_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /encrypted_answers/1
  # DELETE /encrypted_answers/1.json
  def destroy
    @encrypted_answer.destroy
    respond_to do |format|
      format.html { redirect_to encrypted_answers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_encrypted_answer
      @encrypted_answer = EncryptedAnswer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def encrypted_answer_params
      params[:encrypted_answer]
    end
end
