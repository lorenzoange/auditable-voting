class CastVotesController < ApplicationController
  before_action :set_cast_vote, only: [:show, :edit, :update, :destroy]

  # GET /cast_votes
  # GET /cast_votes.json
  def index
    @cast_votes = CastVote.all
  end

  # GET /cast_votes/1
  # GET /cast_votes/1.json
  def show
  end

  # GET /cast_votes/new
  def new
    @cast_vote = CastVote.new
  end

  # GET /cast_votes/1/edit
  def edit
  end

  # POST /cast_votes
  # POST /cast_votes.json
  def create
    @cast_vote = CastVote.new(cast_vote_params)

    respond_to do |format|
      if @cast_vote.save
        format.html { redirect_to @cast_vote, notice: 'Cast vote was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cast_vote }
      else
        format.html { render action: 'new' }
        format.json { render json: @cast_vote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cast_votes/1
  # PATCH/PUT /cast_votes/1.json
  def update
    respond_to do |format|
      if @cast_vote.update(cast_vote_params)
        format.html { redirect_to @cast_vote, notice: 'Cast vote was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cast_vote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cast_votes/1
  # DELETE /cast_votes/1.json
  def destroy
    @cast_vote.destroy
    respond_to do |format|
      format.html { redirect_to cast_votes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cast_vote
      @cast_vote = CastVote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cast_vote_params
      params.require(:cast_vote).permit(:cast_at, :vote_hash, :voter_hash, :voter_uuid)
    end
end
