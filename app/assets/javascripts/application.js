// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

$( document ).ready(function() {
 
    var n_questions = 0;
    $("#add_question").click(function(e){ 
        e.preventDefault();
        var question_name = "<label for=\"question_"+n_questions+"_name\">Name</label>"+
                            "<input id=\"question_"+n_questions+"_name\" name=\"question_"+n_questions+"_name\" type=\"text\" />";
        
        var question_short_name = "<label for=\"question_"+n_questions+"_short_name\">Short name</label>"+
                                  "<input id=\"question_"+n_questions+"_short_name\" name=\"question_"+n_questions+"_short_name\" type=\"text\" />";
        
        var question_min = "<label for=\"question_"+n_questions+"_min\">Min selected answers</label>"+
                                  "<input id=\"question_"+n_questions+"_min\" name=\"question_"+n_questions+"_min\" type=\"text\" size=\"2\" />";
                                  
        var question_max = "<label for=\"question_"+n_questions+"_max\">Max selected answers</label>"+
                                  "<input id=\"question_"+n_questions+"_max\" name=\"question_"+n_questions+"_max\" type=\"text\" size=\"2\" />";
                                  
        var add_answer = $('<button/>', {   text: "Add answer",
                                            click: add_answer_click
                                        });
        add_answer.data( "add-answer-to", n_questions);

        var n_answers = "<input id=\"question_"+n_questions+"_n_possible_answers\" name=\"question_"+n_questions+"_n_possible_answers\" type=\"hidden\" value=\"0\" />";
                
        $(this).parent('form').append( $('<div/>', { id: "question_"+n_questions }) );
        
        $("#question_"+n_questions).append( "<strong>Question "+n_questions+"</strong>", 
                                            question_name, question_short_name, 
                                            question_min, question_max, 
                                            add_answer, n_answers );
        
        n_questions++;
        $("#n_questions").attr("value", n_questions);
    });
 
    var n_possible_answers = [];
    var add_answer_click = function(e){ 
        e.preventDefault();
        n_question = parseInt( $(this).data("add-answer-to") );
        
        if( n_possible_answers[ n_question ] == undefined ){
          n_possible_answers[ n_question ] = 0;
        }
        
        n_possible_answer = n_possible_answers[ n_question ];
        
        var possible_answer_answer = "<label for=\"possible_answer_"+n_question+"_"+n_possible_answer+"_answer\">Answer "+n_possible_answer+"</label>"+
                                     "<input id=\"possible_answer_"+n_question+"_"+n_possible_answer+"_answer\" name=\"possible_answer_"+n_question+"_"+n_possible_answer+"_answer\" type=\"text\" />";
        
        var possible_answer_url = "<label for=\"possible_answer_"+n_question+"_"+n_possible_answer+"_url\">Answer "+n_possible_answer+" URL</label>"+
                                  "<input id=\"possible_answer_"+n_question+"_"+n_possible_answer+"_url\" name=\"possible_answer_"+n_question+"_"+n_possible_answer+"_url\" type=\"text\" />";
        
        $("#question_"+n_question).append( possible_answer_answer, possible_answer_url );
        
        n_possible_answers[ n_question ]++;
        $("#question_"+n_question+"_n_possible_answers").attr("value", n_possible_answers[ n_question ]);
    }
 
});


