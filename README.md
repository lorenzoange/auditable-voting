# Ruby on Rails Auditable Voting System

Il prototipo sviluppato prende spunto in gran parte da Helios, prodotto open source.
Helios è un sistema di E2E Auditable Voting sviluppato dalla Harvard University e da Ben Adida.
Il sistema apre le frontiere a nuove possibiltà di verificare votazioni su internet:
Permette, infatti, a chiunque di creare elezioni, invitare elettori a votare e infine 
computare l'esito generando una prova di validità dell'intero processo.

La motivazione che stà alla base della creazione di un sistema di E2E Auditable Voting è fornire una risposta alla
domanda "Se il mio voto è garantito che rimanga segreto, come faccio a verificare che venga correttamente conteggiato
nel computo dei risultati?".
Questa domanda è una domanda che qualunque elettore dovrebbe porsi.
In un'elezione tradizionale la segretezza del voto è garantita dal fatto che, 
inserendo il voto dentro un'urna, si confonde insieme a quello di altri.
La debolezza del sistema tradizionale, però, è insita nel fatto che non c'è alcuna possibilità 
di verificare che il voto arrivi a destinazone non alterato e venga conteggiato adeguatamente.
è necessario, quindi, fare affidamento sulla correttezza e onestà del sistema elettorale.

In un sistema di voto elettronico è facile mantenere il voto segreto con tecniche piu' o meno 
sofisticate di crittografia ma per quanto riguarda la verificabilità del processo?
Questa è la sfida che questa tesi si è preposta di affrontare:
Garantire segretezza del voto e verificabilità del processo.
Non è necessario, infatti, fidarsi della piattaforma e dei suoi componenti ma è il sistema a fornire prove matematiche
inconfutabili che garantiscono la correttezza del tutto.
Per questo motivo il prototipo è definibile come sistema di E2E Auditable Voting.
